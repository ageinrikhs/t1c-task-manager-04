# TASK-MANAGER

## DEVELOPER

**NAME**: Alexander Geinrikhs

**E-mail**: ageinrikhs@t1-consulting.ru

**E-mail**: ageinrihs@vtb.ru

## SOFTWARE

**Java**: JDK 1.8

**OS**: Windows 10 Corporate

## HARDWARE

**CPU**: Intel Core i5

**RAM**: 16 Gb

**SSD**: 512 Gb

## APPLICATION RUN

```shell
java -jar ./task-manager.jar
```
