package ru.t1c.ageinrikhs.tm;

import static ru.t1c.ageinrikhs.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            showArgumentError();
            return;
        }
        final String arg = args[0];
        switch (arg) {
            case VERSION:
                showVersion();
                break;
            case ABOUT:
                showAbout();
                break;
            case HELP:
                showHelp();
                break;
            default:
                showArgumentError();
        }
    }

    public static void showArgumentError() {
        System.err.println("Error! This argument is not supported");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.1");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexander Geinrikhs");
        System.out.println("E-mail: ageinrikhs@t1-consulitng.ru");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - show program version. \n", VERSION);
        System.out.printf("%s - show developer info. \n", ABOUT);
        System.out.printf("%s - show list of available commands. \n", HELP);
    }
}
